close all 
clear all

% Initialisation des parametres de simulation :
% Va :Vitesse d'avance, Lr : Largeur du bois, Lg : Longueur du bois, 
% h : Hauteur du bois, Te : Periode des itérations.

Va = 0.05  ; Lr = 0.5 ; Lg = 1 ; h = 0.1 ; Te = 10^(-1) ; 

% Initialisation des params de frottement

% Coef de frottement visqueux
K = 30                                                  ;

% Matrice de coef de frottement
Ka = -[K , 0 , 0;
       0 , K , 0;
       0 , 0 , 0]                                       ;   

% Initialisation du vecteur vitesse
V = [ Va ; 0 ; 0]                                       ;  
% Initialisation de la vitesse désirée
Vdes = V                                                ; 


% Initialisation des parametres de coupe :
% Phi : Angle entre la vitesse de coupe et le grain du bois
% D: Densité du bois

phi = 1.57 ; D = 400                                    ;

% Efforts fournits par la toupie
Ws = -Shaper(phi,D)                                      ;


% Initialisation des paramètres nécéssaire au calcule des matrices de Grasp:
% Positions initiales des points d'applications des efforts et de l'origine 
 
Cl = [ -0.8 , 0.45 , 0 ]  ;    Cr = [ -0.95 , 0.2 , 0 ]   ;   Cs = [ -0.05 , 0.05 , 0 ]   ;  P  = [ 0 , 0 , 0 ]   ;          
Xli = Cl(1) ; Yli = Cl(2) ; Xri = Cr(1) ; Yri = Cr(2) ; Xsi = Cs(1) ; Ysi = Cs(2) ;

% Matrice de sélection des DDLs 
Sel = [ 1 , 0 , 0 , 0 , 0 , 0 ;
        0 , 1 , 0 , 0 , 0 , 0 ;
        0 , 0 , 0 , 0 , 0 , 1 ]                    ;

% Matrice de rotaion entre le repère contact et le repère galiléen
R  =  diag(ones(3,1))                              ;


% MAtrice de grasp initiale après sélection des DDLs de la main droite
Gr = Grasp(Cr,P,Sel,R)                             ;

% MAtrice de grasp initiale après sélection des DDLs de la main gauche
Gl = Grasp(Cl,P,Sel,R)                             ;

%MAtrice de grasp après sélection des DDLs de la toupie
Gs = Grasp(Cs,P,Sel,R)                             ;



% Initialisation des paramètres d'optimisations

% Matrice de pondération de la norme des efforts mains droite et gauche
Wei    = eye(6)                                    ;
Wei(1,1) = 0.8 ; Wei(2,2) = 0.4 ; Wei(3,3) = 0.4     ; 
Wei(4,4) = 0.3 ; Wei(5,5) = 0.5 ; Wei(6,6) = 0.4   ; 

% Matrice des contraintes d'inégalités (Selection des variable et du signe de l'inégalité) 
Ainq = -[    1                       , zeros(1,5)  ;  %x1
          zeros(1,3)    ,     1      , zeros(1,2)  ]; %x4  
      
% Matrice de contrainte d'inégalité bornes 
binq = [zeros(2,1) ]                               ;

% Matrice des contraintes d'égalité (Multiplié par les variable) 
Aeq = [ Gl , Gr ]                                  ;

% Matrice de contrainte d'égalité 
Gant = [ Gs , Ka]                                  ;
Want = [ Ws ; Vdes ]                               ;
beq  = -Gant * Want                                ;

% Matrice Initiale fct de cout
G    =  Wei' * Wei                                 ;

% Vecteur Initial fct de cout
p    =  zeros(6,1)                                 ;


% Initialisation du déplacement de la pièce par itération
TrX(1) = Va*Te ; TrY(1) = 0                                      ;
Dx(1) = 0 ; Dy(1) = 0                                            ; 
i = 1                                                            ;

% Boucle de mise à jour de la position de la pièce et des efforts optimisés
while(Dx(i) < 1)
    
i = i + 1                                                        ;

% Génération du vecteur temps
Tm(i) = i * Te                                                   ;

% Mise à jour des matrice des points d'application des efforts    
Cl(1) = Cl(1) + TrX(i-1) ; Cr(1) = Cr(1) + TrX(i-1)              ;
Cl(2) = Cl(2) + TrY(i-1) ; Cr(2) = Cr(2) + TrY(i-1)              ; 


% Mise à jour des matrice de grasp   
Gr = Grasp(Cr,P,Sel,R)   ;   Gl = Grasp(Cl,P,Sel,R)              ;


% Mise à jour de la matrice d'égalité
Aeq = [ Gl , Gr ]                                                ;


% Résolution du problème par LQP
[x ,fval] = quadprog(G,p,Ainq,binq,Aeq,beq)                      ;



% Efforts fournis par la main gauche
Wlx(i) =  x(1)                                                   ;
Wly(i) =  x(2)                                                   ;
Wlt(i) =  x(3)                                                   ;

% Efforts fournis par la main droite
Wrx(i) =  x(4)                                                   ;
Wry(i) =  x(5)                                                   ;
Wrt(i) =  x(6)                                                   ;

% Calcule de la vitesse d'avance
Vx(i) = ( Wrx(i) + Wlx(i) + Ws(1) )/K                            ;
Vy(i) = ( Wry(i) + Wly(i) + Ws(2) )/K                            ;

% Déplacement de la pièce par itération
TrX(i) = Vx(i) * Te                                              ;
TrY(i) = Vy(i) * Te                                              ;

% Calcule du déplacement total
Dx(i) = TrX(i) + Dx(i-1)                                         ;
Dy(i) = TrY(i) + Dy(i-1)                                         ;

end

