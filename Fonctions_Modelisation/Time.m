function T = Time(Te,Lg,Va)
Tf = Lg/(Va*Te) ;
for i = 1 : Tf
    T(i) = i ;
end
T=T*Te;
end