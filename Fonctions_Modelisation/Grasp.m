function G = Grasp(C,P,Sel,R)

Px = P(1) ; Py = P(2) ; Pz = P(3)                  ;

Cx = C(1) ; Cy = C(2) ; Cz = C(3)            ;

% Matrice de produit vectoriel
S  = [      0     , -(Cz - Pz) ,  (Cy - Py) ;
        (Cz - Pz) ,       0     , -(Cx - Px) ;         
       -(Cy - Py) ,  (Cx - Px) ,       0     ]   ;
%Matrice de transfert de point d'application de l'effort robot
Pc  = [  diag(ones(3,1)) ,       zeros(3)     ;
               S        ,    diag(ones(3,1)) ]    ;
%Matrice de rotaion entre le repère contact et le repère galiléen                             ;
Rt = [   R    , zeros(3) ;
       zeros(3) ,    R    ]                       ;
%MAtrice de grasp après sélection des DDLs du robot 
G = Sel * (Rt)' * Pc * (Sel)'                    ;


end

