close all 
clear all

% Initialisation des parametres de simulation :
% Va :Vitesse d'avance, Lr : Largeur du bois, Lg : Longueur du bois, 
% h : Hauteur du bois, Te : Periode des itérations.

Va = 0.05  ; Lr = 0.5 ; Lg = 1  ;  h = 0.1  ;  Te = 10^(-2) ; 



% Time: est la fonction indiquant le vecteur de temps durant la simu

Tm  = Time(Te,Lg,Va)                                        ;



% Initialisation des parametres de coupe :
% Phi : Angle entre la vitesse de coupe et le grain du bois
% D: Densité du bois

phi = 1.57 ; D = 400                                        ;

% Efforts fournits par la toupie
Ws = -Shaper(phi,D)                                          ;


% Initialisation des paramètres nécéssaire au calcule des matrices de Grasp:
% Positions initiales des points d'applications des efforts et de l'origine 
 
Cl = [ -0.8 , 0.45 , 0 ]  ;    Cr = [ -0.95 , 0.2 , 0 ]  ;   Ch = [ -0.1 , 0.45 , 0 ]  ;   Cs = [ -0.05 , 0.05 , 0 ]   ;  P  = [ 0 , 0 , 0 ]   ;
Xli = Cl(1) ; Yli = Cl(2) ; Xri = Cr(1) ; Yri = Cr(2) ; Xsi = Cs(1) ; Ysi = Cs(2) ; Xhi = Ch(1) ; Yhi = Ch(2) ;

% Matrice de sélection des DDLs 
Sel = [ 1 , 0 , 0 , 0 , 0 , 0 ;
        0 , 1 , 0 , 0 , 0 , 0 ;
        0 , 0 , 0 , 0 , 0 , 1 ]                    ;

% Matrice de rotaion entre le repère contact et le repère galiléen
R  =  diag(ones(3,1))                              ;

% MAtrice de grasp initiale après sélection des DDLs de la main gauche
Gl = Grasp(Cl,P,Sel,R)                             ;

% MAtrice de grasp initiale après sélection des DDLs de la main droite
Gr = Grasp(Cr,P,Sel,R)                             ;

% MAtrice de grasp initiale après sélection des DDLs du robot
Gh = Grasp(Ch,P,Sel,R)                             ;

% MAtrice de grasp après sélection des DDLs de la toupie
Gs = Grasp(Cs,P,Sel,R)                             ;



% Initialisation des paramètres d'optimisations
% Paramètres de pondération de la fct de régularisation
eps = 10e-6 ;

% Matrice de pondération de la norme des efforts mains droite et gauche
Wei    = eye(6)   ;
Wei(1,1) = 1 ; Wei(2,2) = 0.1 ; Wei(3,3) = 0.4     ; 
Wei(4,4) = 0.1 ; Wei(5,5) = 0.5 ; Wei(6,6) = 0.4   ;
Wei(7,7) = 0.1 ; Wei(8,8) = 0.1 ; Wei(9,9) = 0.4   ;

% Matrice des contraintes d'inégalités (Selection des variable et du signe de l'inégalité) 
Ainq = -[    1                       , zeros(1,8)  ; %x1
          zeros(1,3)    ,     1      , zeros(1,5)  ; %x4  
            -1          ,              zeros(1,8)  ; %x1
          zeros(1,3)    ,    -1      , zeros(1,5)  ; %x4
          zeros(1,2)    ,    -1      , zeros(1,6)  ; %x3
          zeros(1,5)    ,    -1      , zeros(1,3)  ; %x6
          zeros(1,8)    ,                  -1      ; %x9
          zeros(1,2)    ,     1      , zeros(1,6)  ; %x3
          zeros(1,5)    ,    -1      , zeros(1,3)  ; %x6
          zeros(1,8)    ,                  1       ; %x9
          zeros(1,1)    ,    -1      , zeros(1,7)  ; %x2 
          zeros(1,1)    ,     1      , zeros(1,7)  ; %x2
          zeros(1,4)    ,    -1      , zeros(1,4)  ; %x5 
          zeros(1,4)    ,     1      , zeros(1,4)  ];%x5 
      
% Matrice de contrainte d'inégalité bornes 
binq = [0 ; 0 ; 50 ; 150 ; 20 ; 20 ; 20 ; -20 ; -20 ; -20 ; -100 ; 100 ; -30 ; 30]  ;
  
% Matrice Initiale fct de cout
Gsys = [ Gl , Gr , Gh]                             ;
G    = (Gsys)' * Gsys + eps * Wei' * Wei           ;

% Vecteur Initial fct de cout
p    = 2 * Ws' * Gs' * Gsys                       ;

% Calcule du temps de coupe nécessaire
Tf = (Lg/(Va*Te))                                  ;

% Calcule du déplacement de la pièce par itération
TrW = Va*Te                                        ;

% Boucle de mise à jour de la position de la pièce et des efforts optimisés
for i = 1 : Tf

% Mise à jour des matrice des points d'application des efforts    
    
Cl(1) = Cl(1) + TrW ; Cr(1) = Cr(1) + TrW ; Ch(1) = Ch(1) + TrW ; 

% Mise à jour des matrice de grasp   
Gr = Grasp(Cr,P,Sel,R) ; Gl = Grasp(Cl,P,Sel,R)    ;
Gh = Grasp(Ch,P,Sel,R)                             ;

% Mise à jour de la matrice fct cout
Gsys = [ Gl , Gr , Gh ]                            ;
G    = (Gsys)' * Gsys + eps*Wei                    ;

% Mise à jour vecteur fct cout
p    = 2 * Ws' * Gs' * Gsys                       ;


% Résolution du problème par LQP
[x ,fval] = quadprog(G,p,Ainq,binq)                ;

% Efforts fournis par la main gauche
Wlx(i) =  x(1)                                     ;
Wly(i) =  x(2)                                     ;
Wlt(i) =  x(3)                                     ;

% Efforts fournis par la main droite
Wrx(i) =  x(4)                                     ;
Wry(i) =  x(5)                                     ;
Wrt(i) =  x(6)                                     ;

% Efforts fournis par le robot
Whx(i) =  x(7)                                     ;
Why(i) =  x(8)                                     ;
Wht(i) =  x(9)                                     ;

end

