close all

% Figure représentant les efforts appliqués par la main gauche de
% l'opérateur

figure (1)
subplot(3,1,1) 
plot(Tm,Wlx)
grid
title('Evolution de la force appliqué par la main gauche parralèle à Va')
xlabel('Time (S)')
ylabel('Force (N)')
subplot(3,1,2)
plot(Tm,Wly)
grid
title('Evolution de la force appliqué par la main gauche normale à Va ')
xlabel('Time (S)')
ylabel('Force (N)')
subplot(3,1,3) 
plot(Tm,Wlt)
grid
title('Evolution du couple appliqué par la main droite ')
xlabel('Time (S)')
ylabel('Torque (N/m)')


% Figure représentant les efforts appliqués par la main droite de
% l'opérateur
figure (2)
s7 = subplot(3,1,1) 
plot(Tm,Wrx)
xlabel('Time (S)')
ylabel('Force (N)')
title('Evolution de la force appliqué par la main droite parralèle à Va')
grid
s8 = subplot(3,1,2) 
plot(Tm,Wry)
xlabel('Time (S)')
ylabel('Force (N)')
grid
title('Evolution de la force appliqué par la main droite normale à Va ')
s9 = subplot(3,1,3) 
plot(Tm,Wrt)
grid
title('Evolution du couple appliqué par la main gauche ')
xlabel('Time (S)')
ylabel('Torque (N/m)')


% Initialisation du repère et des position initiales du bois et des efforts
figure (3)

axis ( [ -2  2  -2  2  -2  2 ] ) ; daspect( [ 1 , 1 , 1 ] )                                                         ;
hold on
arrow3 ( zeros(3) , diag( [ 1 , 1 , 1 ] ) , '--o' )                                                                         ;
[ Cx , Cy , Cz ] = cylinder( 0.05 , 50 )                                                                                 ;
Cz = 0.1 * (Cz - 1)                                                                                          ;
cd = surf(Cx,Cy,Cz)                                                                                          ;
[C1,C2,C3,C4,C5,C6] = cube(Lr,Lg,h,0,0)                                                                        ;
text(1,0,0,'X'), text(0,1,0,'Y'),text(0,0,1,'Z','VerticalAlignment','bottom','HorizontalAlignment','center') ;
grid
pause
delete(C1) ; delete(C2) ; delete(C3) ; delete(C4) ; delete(C5) ; delete(C6) ;
xlabel('Echelle 100 N ')
ylabel('Echelle 100 N')
zlabel('Echelle 50 N/m')



% Mise à l'echelle des efforts de la toupie
Xsf = Ws(1)/100 ; Ysf = Ws(2)/100

% Tracer des vecteurs d'efforts toupie 
ws = arrow3([ Xsi , Ysi , 0 ],[ Xsf , Ysf , 0 ] , '2m' , 0.5 , [] , 0 )
text(Xsi - 0.2 ,Ysi - 0.2,'Ws','FontSize',14,'Color','magenta')

% Boucle de mise à jour des efforts et du déplacement du bois
for q = 1 : (i/25)
 
% Mettre à  jour les valeurs toutes les 25 itérations
f = q * 25                                                                                          ;
Dx =   TrW * f                                                                                      ;  

% Mise à jour et mise à l'échelle des efforts fournies par l'opérateur
Xrf = Wrx(f)/100 ; Yrf = Wry(f)/100 ; Zrf = Wrt(f)/50 ; Xlf = Wlx(f)/100 ; Ylf = Wly(f)/100 ; Zlf = Wlt(f)/50 ; 

% Mise à jour du déplacemnt du bois
[C1,C2,C3,C4,C5,C6] = cube(Lr,Lg,h,Dx,0)  
txr = text(Xri + Dx - 0.3 ,Yri,'Wr','FontSize',14,'Color','green')                                  ;
txl = text(Xli + Dx - 0.1 ,Yli + 0.2,'Wl','FontSize',14,'Color','black')                            ;


% Tracer des vecteurs d'efforts la main droite 
wr  = arrow3([ Xri + Dx , Yri , 0 ] , [ Xrf + Xri + Dx ,Yrf + Yri , 0 ] , '2g' , 0.5 , [] , 0 )     ;
wrz = arrow3([ Xri + Dx , Yri,0],[ Xri + Dx , Yri , Zrf ],'2g',0.5,[],0)                            ;

% Tracer des vecteurs d'efforts la main gauche 
wl  = arrow3([Xli + Dx , Yli , 0 ],[Xlf + Xli + Dx ,Ylf + Yli , 0 ] , '2k' , 0.5 , [] , 0 )         ;
wlz = arrow3([Xli + Dx , Yli ,0],[Xli + Dx , Yli , Zlf ] , '2k',0.5,[],0)                           ;                           

% Mise à l'échelle du temps d'affichage
pause(0.25)

% Effacer les vecteur de l'itération précédente
delete(wr)                      ;
delete(wrz)                     ;
delete(txr)                     ;
delete(wl)                      ;
delete(wlz)                     ;
delete(txl)                     ;

delete(C1) ; delete(C2) ; delete(C3) ; delete(C4) ; delete(C5) ; delete(C6) ;


end

% Affichage des dernière valeurs calculées

[C1,C2,C3,C4,C5,C6] = cube(Lr,Lg,h,Dx,0)  
txr = text(Xri + Dx - 0.3 ,Yri,'Wr','FontSize',14,'Color','green'), txl = text(Xli + Dx - 0.1 ,Yli + 0.2,'Wl','FontSize',14,'Color','black')


wr  = arrow3([ Xri + Dx , Yri , 0 ] , [ Xrf + Xri + Dx ,Yrf + Yri , 0 ] , '2g' , 0.5 , [] , 0 )
wrz = arrow3([ Xri + Dx , Yri,0],[ Xri + Dx , Yri , Zrf ],'2g',0.5,[],0)

wl  = arrow3([Xli + Dx , Yli , 0 ],[Xlf + Xli + Dx ,Ylf + Yli , 0 ] , '2k' , 0.5 , [] , 0 )
wlz = arrow3([Xli + Dx , Yli ,0],[Xli + Dx , Yli , Zlf ] , '2k',0.5,[],0)
