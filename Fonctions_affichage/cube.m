function [C1,C2,C3,C4,C5,C6] = cube(Lr,Lg,h,Dx,Dy)
[X Y] = meshgrid(linspace(-Lg+Dx,Dx,2),linspace(Dy,Lr+Dy,2));
Z = meshgrid(linspace(-h,0,2));
I = ones(length(X),length(Y));
hold on
C6 = surf(X,Y,I*0)
C1 = surf(X,Y,-I*h) 
C2 = surf((-Lg + Dx ) * I , Y , Z ) 
C3 = surf(Dx * I , Y , Z ) 
C4 = surf( X  , Dy * I  , Z' ) 
C5 = surf( X  , (Lr+Dy) * I  , Z' ) 
end